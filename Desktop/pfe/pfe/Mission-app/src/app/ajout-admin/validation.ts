export const formError = {
    
    'cin': [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'nom' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
    'prenom' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],

    'email' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
        {type:'pattern',message:'Email invalide'}
    ],
    'login' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'mot_de_passe' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
};