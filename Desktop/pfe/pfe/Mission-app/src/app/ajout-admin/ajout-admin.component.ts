import { Component, OnInit } from '@angular/core';
import { AgentService } from '../Services/agent.service';
import { Agent } from '../Model/agent';
import { FormBuilder,Validators,FormGroup,FormControl } from '@angular/forms';
import {formError} from './validation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-admin',
  templateUrl: './ajout-admin.component.html',
  styleUrls: ['./ajout-admin.component.css']
})
export class AjoutAdminComponent implements OnInit {
agent : Agent = new Agent();

f: FormGroup;
message: string;
  validation_messages: any ;
  constructor( private formBuilder: FormBuilder, private agentservice: AgentService, private route: Router) {
    this.validation_messages = formError;
    this.f = this.formBuilder.group({
    cin: ['', Validators.required], nom: ['', Validators.required], prenom: ['', Validators.required], 
      login: ['', Validators.required],  mot_de_passe: ['', Validators.required],
      email: new FormControl('' , Validators.compose([Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),Validators.required])),

    });
  }

  ngOnInit() {    
  }
/*newAgent(): void{
  this.agent = new Agent();
}*/


save(){
  this.agentservice.createAgent(this.agent).subscribe(
    data => {
    console.log(data);
    alert('Vous avez ajouté Agent avec succés!');
this.route.navigateByUrl("/admin");}
     ,error => {
      console.log(error);
      alert("Probléme Ajout");}
    
  );

  /*this.agent = new Agent();*/


}


onSubmit(){
  if (this.f.invalid) {
    Object.keys(this.f.controls).forEach(field => {
      const control = this.f.get(field);
      control.markAsTouched({onlySelf: true});
  })
}
if(this.f.valid){
  this.save();

}

}
}