import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarreLeftComponent } from './barre-left.component';

describe('BarreLeftComponent', () => {
  let component: BarreLeftComponent;
  let fixture: ComponentFixture<BarreLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarreLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarreLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
