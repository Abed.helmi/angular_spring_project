import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../Authentification/auth.service';
@Component({
  selector: 'app-barre-left',
  templateUrl: './barre-left.component.html',
  styleUrls: ['./barre-left.component.css']
})
export class BarreLeftComponent implements OnInit {

  constructor(
    private router: Router,private authservice: AuthService) {

  }
  ngOnInit() {
  }
  toHome(){
    this.router.navigateByUrl('/home');
   }

   logout(){
    let confirme=window.confirm('Voulez vous vraiment déconnecter');
    if(confirme==true){   
       this.authservice.logOut();
    this.router.navigateByUrl('/auth');
  }
}
}
