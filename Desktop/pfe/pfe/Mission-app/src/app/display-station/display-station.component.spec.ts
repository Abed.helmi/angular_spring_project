import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayStationComponent } from './display-station.component';

describe('DisplayStationComponent', () => {
  let component: DisplayStationComponent;
  let fixture: ComponentFixture<DisplayStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
