import { Component, OnInit } from '@angular/core';
import { Station } from '../Model/station';
import { Router } from '@angular/router';
import { StationService } from '../Services/station.service';

@Component({
  selector: 'app-display-station',
  templateUrl: './display-station.component.html',
  styleUrls: ['./display-station.component.css']
})
export class DisplayStationComponent implements OnInit {

  stations: Array<Station[]>;
  motcle:string="";
  constructor(  private route: Router, private statuionservice: StationService) { }

  ngOnInit() {
    this.display();  }

display(){
  this.statuionservice.getStationList().subscribe(
    data => { this.stations = data;}
  );
}




deleteStation(id_station: number){
  let confirme=window.confirm('Est vous sure ?');


  if(confirme==true){

    this.statuionservice.deleteStation(id_station).subscribe(
      data =>{
        console.log(data);
        confirme=window.confirm('Vous avez supprimé Station avec succès!');

        this.ngOnInit();

  
      },
      error => {
        alert("problème de supprussion");
  
      }
    );
    }
   }

    newStation(){
      let station = new Station();
      this.statuionservice.setter(station);
      this.route.navigateByUrl('home/gererstations/consulterstation/modifierstation');
    }

    UpdateStation(chauffeur){
    this.statuionservice.setter(chauffeur);
    this.route.navigateByUrl('/home/gererstations/consulterstation/modifierstation');
  }

  dosearch(){
    this.statuionservice.getStationch(this.motcle).subscribe(data=>{this.stations=data;
    console.log(this.stations);
    }
     ,err=>{console.log("erreur")});
    
    
  }
  
  chercher(){
  this.dosearch();
  }

}
