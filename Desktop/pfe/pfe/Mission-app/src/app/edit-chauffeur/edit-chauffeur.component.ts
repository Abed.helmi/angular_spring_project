import { Component, OnInit } from '@angular/core';
import { Chauffeur } from '../Model/chauffeur';
import { ChauffeurService } from '../Services/chauffeur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-chauffeur',
  templateUrl: './edit-chauffeur.component.html',
  styleUrls: ['./edit-chauffeur.component.css']
})
export class EditChauffeurComponent implements OnInit {

  chauffeur : Chauffeur = new Chauffeur();
  constructor(   private chauffeurservice : ChauffeurService, private route: Router) { }

  ngOnInit() {

    this.chauffeur = this.chauffeurservice.getter();
  }

  save(){
    this.chauffeurservice.createChauffeur(this.chauffeur).subscribe(
      data => {console.log(data);
      alert("Mise à jour effectuée ");
      this.route.navigateByUrl('/home/gererchaufferes/consulterchauffeur');

    },
       error => {
        
      console.log(error);
      alert("Problème de modification");
              
       }
    );   
    }



  retour(){
    this.route.navigateByUrl("/home/gererchaufferes/consulterchauffeur");

  }


  onSubmit()  {
    this.save();


  }


}
