import { Component, OnInit } from '@angular/core';
import { Station } from '../Model/station';
import { Router } from '@angular/router';
import { StationService } from '../Services/station.service';

@Component({
  selector: 'app-edit-station',
  templateUrl: './edit-station.component.html',
  styleUrls: ['./edit-station.component.css']
})
export class EditStationComponent implements OnInit {
  station : Station = new Station();

  constructor( private route: Router, private stationservice: StationService) { }

  ngOnInit() {


    this.station = this.stationservice.getter();

  }




  save(){
    this.stationservice.createSattion(this.station).subscribe(
      data => {console.log(data);
      alert("Mise à jour effectuée ");
      this.route.navigateByUrl('/home/gererstations/consulterstation');

    },
       error => {
        
      console.log(error);
      alert("Problème de modification");
              
       }
    );   
    }



  retour(){
    this.route.navigateByUrl("/home/gererstations/consulterstation");

  }


  onSubmit()  {
    this.save();


  }

}
