import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GereChauffComponent } from './gere-chauff.component';

describe('GereChauffComponent', () => {
  let component: GereChauffComponent;
  let fixture: ComponentFixture<GereChauffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GereChauffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GereChauffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
