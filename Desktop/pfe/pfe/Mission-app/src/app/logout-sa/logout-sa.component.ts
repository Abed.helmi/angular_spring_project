import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../Authentification/auth.service';

@Component({
  selector: 'app-logout-sa',
  templateUrl: './logout-sa.component.html',
  styleUrls: ['./logout-sa.component.css']
})
export class LogoutSaComponent implements OnInit {

  constructor( private route: Router, private authservice: AuthService) { }

  ngOnInit() {

  }
  logout(){

    let confirme=window.confirm('Voulez vous vraiment déconnecter ?');
    if(confirme==true){
  this.authservice.logOut();
  this.route.navigateByUrl('/auth');
}
  }
  
}
