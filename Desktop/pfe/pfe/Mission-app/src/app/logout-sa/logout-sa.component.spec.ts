import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutSaComponent } from './logout-sa.component';

describe('LogoutSaComponent', () => {
  let component: LogoutSaComponent;
  let fixture: ComponentFixture<LogoutSaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutSaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutSaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
