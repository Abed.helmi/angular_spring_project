import { Component, OnInit } from '@angular/core';
import { Station } from '../Model/station';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {formError} from './validation';
import { Router } from '@angular/router';
import { StationService } from '../Services/station.service';
@Component({
  selector: 'app-ajout-station',
  templateUrl: './ajout-station.component.html',
  styleUrls: ['./ajout-station.component.css']
})
export class AjoutStationComponent implements OnInit {
  station : Station = new Station();


  f: FormGroup;
  message: string;
  validation_messages: any ;
  constructor( private formbuilder : FormBuilder,private statioservice : StationService, private route : Router) { 

    this.validation_messages = formError;
    this.f = this.formbuilder.group({
      adresse: ['', Validators.required], longitude: ['', Validators.required], altitude: ['', Validators.required],nom: ['', Validators.required]
  
    });


  }

  ngOnInit() {
  }

  save(){
    this.statioservice.createSattion(this.station).subscribe(
      data => {
        console.log(data);
        alert("Vous avez ajouté Station avec succés !");
        this.route.navigateByUrl("/home/gererstations");
      }, error => {
        alert("Problème Ajout")
      }
    )   ;
  }


  onSubmit(){
    if (this.f.invalid) {
      Object.keys(this.f.controls).forEach(field => {
        const control = this.f.get(field);
        control.markAsTouched({onlySelf: true});
    });
    } 
   if(this.f.valid){
     this.save();
   }
  }


}
