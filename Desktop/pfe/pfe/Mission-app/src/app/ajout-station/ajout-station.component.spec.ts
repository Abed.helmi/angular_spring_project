import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutStationComponent } from './ajout-station.component';

describe('AjoutStationComponent', () => {
  let component: AjoutStationComponent;
  let fixture: ComponentFixture<AjoutStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
