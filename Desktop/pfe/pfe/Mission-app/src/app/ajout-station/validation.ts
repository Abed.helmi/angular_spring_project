export const formError = {
    'adresse': [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'longitude' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
    'altitude' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'nom' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ]
};