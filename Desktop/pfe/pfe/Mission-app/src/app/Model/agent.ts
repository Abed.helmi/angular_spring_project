
export class Agent {
    id: number;
    cin:number;
    nom: string;
    prenom: string;
     email : string;
     login : string;
     mot_de_passe : string;
     tel: number;
     date_creation: Date;
}