export class Chauffeur {
    id_chauffeur: number;
    cin:number;
    nom_chauffeur: string;
    prenom_chauffeur : string;
    email_chauffeur : string;
    mot_de_passe : string;
    login:string;
    tel:number;
    sang_groupe: String;
    
}