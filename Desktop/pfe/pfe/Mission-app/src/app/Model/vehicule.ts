export class Vehicule {
    id_vehicule: number;
    marque:number;
    matricule: string;
    chemin_gris_carte: string;
    model : string;
    sous_marque : string;
    etat: string;

}