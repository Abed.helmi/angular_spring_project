
export class Mission {
    id_mission: number
    date_debut = new Date();
    date_arrive = new Date();
    chauffeur_id:number;
    vehicule_id: number;  
    id_station_depart:number;    
    id_station_arrive:number;
}