import { Component, OnInit } from '@angular/core';
import { AgentService } from '../Services/agent.service';
import { Agent } from '../Model/agent';
import { Router} from '@angular/router'
@Component({
  selector: 'app-display-admin',
  templateUrl: './display-admin.component.html',
  styleUrls: ['./display-admin.component.css']
})
export class DisplayAdminComponent implements OnInit {
  

  agents: Array<Agent[]>;
 
  motcle:string="";

  constructor( private agentservice: AgentService, private route: Router) { }

  ngOnInit() {
this.display();

  }
display(){
  this.agentservice.getAgentList().subscribe(
    data => {this.agents = data;}
  );
}

  deleteAgent(id: number){
    let confirme=window.confirm('Est vous sure ?');
    if(confirme==true){

  this.agentservice.delteAgent(id).subscribe(
    data =>{
      console.log(data);

      this.ngOnInit();

    },
    error => console.log(error));
    confirme=window.confirm('Vous avez supprimé agent avec succès!');

    }

}

updateAgent(agent){
  this.agentservice.setter(agent);
  this.route.navigateByUrl('/admin/consulterAgents/modifierAgents');
}

/*newAgent(){
  let agent = new Agent();
  this.agentservice.setter(agent);
  this.route.navigateByUrl('/superadmin/consulterAgents/modifierAgents');
}*/


dosearch(){
  this.agentservice.getAgentch(this.motcle).subscribe(data=>{this.agents=data;
  console.log(this.agents);
  }
   ,err=>{console.log("erreur")});
  
  
}

chercher(){
this.dosearch();

}

}
