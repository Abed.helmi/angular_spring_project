export const formError = {

    'date_debut' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
    'date_arrive' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'chauffeur_id' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'vehicule_id' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'id_station_depart' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'id_station_arrive' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ]
    
};