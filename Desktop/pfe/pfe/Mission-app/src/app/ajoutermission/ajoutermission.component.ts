import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {formError} from './validation';
import { Mission } from '../Model/mission';
import { MissionService } from '../Services/mission.service';
@Component({
  selector: 'app-ajoutermission',
  templateUrl: './ajoutermission.component.html',
  styleUrls: ['./ajoutermission.component.css']
})
export class AjoutermissionComponent implements OnInit {
 

  mission : Mission = new Mission();
  f: FormGroup;
  message: string;
  validation_messages: any ;
  constructor(private formBuilder: FormBuilder, private router: Router ,private missionservice: MissionService) { }

  ngOnInit() {

    this.validation_messages = formError;
    this.f = this.formBuilder.group({
    date_debut: ['', Validators.required], 
      date_arrive: ['', Validators.required], chauffeur_id : ['', Validators.required], vehicule_id: ['', Validators.required], 
      id_station_depart: ['', Validators.required], id_station_arrive: ['', Validators.required]
 
    });
  }


  newMission():void{
    this.mission = new Mission();
  }




save(){
  this.missionservice.createMission(this.mission).subscribe(
    data => {
      console.log(data);
      alert("Vous avez ajouté Mission avec succés!");
      this.router.navigateByUrl("/home/planifiermission");}

      , error =>{
       // console.log(JSON.parse(error._body).message);
        alert(" problème Ajout");

      } 
    
  );
  //this.mission = new Mission();

}

  onSubmit(){

    if (this.f.invalid) {
      Object.keys(this.f.controls).forEach(field => {
        const control = this.f.get(field);
        control.markAsTouched({onlySelf: true});
    });
    } 

if(this.f.valid){
  this.save();

  
}
  }

}
