import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutermissionComponent } from './ajoutermission.component';

describe('AjoutermissionComponent', () => {
  let component: AjoutermissionComponent;
  let fixture: ComponentFixture<AjoutermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
