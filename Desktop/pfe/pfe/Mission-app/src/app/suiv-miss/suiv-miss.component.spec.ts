import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuivMissComponent } from './suiv-miss.component';

describe('SuivMissComponent', () => {
  let component: SuivMissComponent;
  let fixture: ComponentFixture<SuivMissComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuivMissComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuivMissComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
