import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatpassService {
  private baseUrl = 'http://localhost:8080/api/v1/stationspassages';

  constructor( private http: HttpClient) { }



  create(stationpass: Object):Observable<Object>{
    return this.http.post(`${this.baseUrl}`,stationpass);
  }


  getstatpasslist(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
   
  }

   
  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}
