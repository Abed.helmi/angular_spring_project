import { TestBed } from '@angular/core/testing';

import { StatpassService } from './statpass.service';

describe('StatpassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatpassService = TestBed.get(StatpassService);
    expect(service).toBeTruthy();
  });
});
