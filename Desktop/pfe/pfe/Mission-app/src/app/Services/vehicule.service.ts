import { Injectable } from '@angular/core';
import { Vehicule } from '../Model/vehicule';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehiculeService {
  private baseUrl = 'http://localhost:8080/api/v1/vehicules';

  private vehicule: Vehicule;
  constructor(private http: HttpClient) { }



  createVehicule(vehicule: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, vehicule);

  }


  getVehiculeList(): Observable<any> {
  return this.http.get(`${this.baseUrl}`);
 
}

getvehicule(motcle:string):Observable<any> 
{

  return this.http.get("http://localhost:8080/api/v1/cherchervehicules?mc="+motcle);
  
}



UpdateVehicule(id_vehicule:number, value: any ):Observable<Object>
{
  return this.http.put(`${this.baseUrl}/${id_vehicule}`, value);

}

deleteVehicule(id_vehicule: number): Observable<any> {
  return this.http.delete(`${this.baseUrl}/${id_vehicule}`, { responseType: 'text' });
}

setter(vehicule: Vehicule){
  this.vehicule = vehicule;
}
getter(){
  return this.vehicule;
}
}
