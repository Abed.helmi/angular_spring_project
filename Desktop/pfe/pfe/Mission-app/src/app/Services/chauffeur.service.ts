import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chauffeur } from '../Model/chauffeur';

@Injectable({
  providedIn: 'root'
})
export class ChauffeurService {
  private baseUrl = 'http://localhost:8080/api/v1/chauffeurs';

  private chauffeur : Chauffeur;
  constructor( private http: HttpClient) { }


  createChauffeur(chauffeur: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, chauffeur);
  

  }


  getChauffeurList(): Observable<any> {
  return this.http.get(`${this.baseUrl}`);
 
}


getChauffch(motcle : string):Observable<any> 
{

  return this.http.get("http://localhost:8080/api/v1/chercherchauffeurs?mc="+motcle);
  
}


deleteChauffeur(id_chauffeur: number): Observable<any> {
  return this.http.delete(`${this.baseUrl}/${id_chauffeur}`, { responseType: 'text' });
}


UpdateChauffeur(id_chauffeur:number, value: any ):Observable<Object>
{
  return this.http.put(`${this.baseUrl}/${id_chauffeur}`, value);

}


setter(chauffeur: Chauffeur){
  this.chauffeur = chauffeur;
}
getter(){
  return this.chauffeur;
}
}
