import { Injectable } from '@angular/core';
import { Mission } from '../Model/mission';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  private baseUrl = 'http://localhost:8080/api/v1/missions';

 private mission : Mission;
  constructor( private http:HttpClient) { }

  createMission(mission:object):Observable<object>{
    return this.http.post(`${this.baseUrl}`, mission);
  }






  getMissionList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
   
  }
  
  deleteMission(id_mission: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id_mission}`, { responseType: 'text' });
  }
  
  
  UpdateMission(id_mission:number, value: any ):Observable<Object>
  {
    return this.http.put(`${this.baseUrl}/${id_mission}`, value);
  

  }
  getMissionch(motcle : string):Observable<any> 
{

  return this.http.get("http://localhost:8080/api/v1/cherchermissions?mc="+motcle);
  
}



setter(mission: Mission){
  this.mission = mission;
}
getter(){
  return this.mission;
}


}
