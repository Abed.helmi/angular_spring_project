import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Station } from '../Model/station';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StationService {

  constructor( private http : HttpClient) { }


  private baseUrl = 'http://localhost:8080/api/v1/stations';

  private station: Station;

  createSattion(station: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, station);

  }

  getStationList(): Observable<any> {
  return this.http.get(`${this.baseUrl}`);
 
}


getStationch(motcle : string):Observable<any> 
{

  return this.http.get("http://localhost:8080/api/v1/chercherstations?mc="+motcle);
  
}



deleteStation(id_station: number): Observable<any> {
  return this.http.delete(`${this.baseUrl}/${id_station}`, { responseType: 'text' });
}

UpdateStation(id_station:number, value: any ):Observable<Object>
{
  return this.http.put(`${this.baseUrl}/${id_station}`, value);

}
setter(station: Station){
  this.station = station;
}
getter(){
  return this.station;
}
}
