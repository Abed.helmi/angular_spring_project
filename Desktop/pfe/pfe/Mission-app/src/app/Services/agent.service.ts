import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Agent } from '../Model/agent';

@Injectable({
  providedIn: 'root'
})
export class AgentService {
  private baseUrl = 'http://localhost:8080/api/v1/agents';

  private agent: Agent;
  constructor( private http: HttpClient) { }

  createAgent(agent: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, agent);

  }

 getAgentList(): Observable<any> {
  return this.http.get(`${this.baseUrl}`);
 
}


getAgentch(motcle : string):Observable<any> 
{

  return this.http.get("http://localhost:8080/api/v1/chercheragents?mc="+motcle);
  
}



delteAgent(id: number): Observable<any> {
  return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
}

UpdateAgent(id:number, value: any ):Observable<Object>
{
  return this.http.put(`${this.baseUrl}/${id}`, value);

}
setter(agent: Agent){
  this.agent = agent;
}
getter(){
  return this.agent;
}
}
