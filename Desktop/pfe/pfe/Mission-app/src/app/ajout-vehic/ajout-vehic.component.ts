import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {formError} from './validation';
import { Vehicule } from '../Model/vehicule';
import { Router } from '@angular/router';
import { VehiculeService } from '../Services/vehicule.service';

@Component({
  selector: 'app-ajout-vehic',
  templateUrl: './ajout-vehic.component.html',
  styleUrls: ['./ajout-vehic.component.css']
})
export class AjoutVehicComponent implements OnInit {


vehicule : Vehicule = new Vehicule();

  f: FormGroup;
  message: string;
  validation_messages: any ;
  constructor( private formBuilder : FormBuilder, private route: Router, private  vehiculeservice: VehiculeService) {

    this.validation_messages = formError;
    this.f = this.formBuilder.group({
    marque: ['', Validators.required], matricule: ['', Validators.required], 
  
    });
   }

  ngOnInit() {

  }


  /*newVehicule(): void{
    this.vehicule = new Vehicule();
  }*/



  save(){
    this.vehiculeservice.createVehicule(this.vehicule).subscribe(
      data => 
      {console.log(data);
      alert("Vous avez ajouté Vehicule avec succés!");
      this.route.navigateByUrl("/home/gerervéhicules");


    }  , error => {

      /*console.log(JSON.parse(error._body).message);*/
      alert("Problème Ajout");
  }

      
    );
    //this.vehicule = new Vehicule();
  
  }

  onSubmit(){

    if (this.f.invalid) {
      Object.keys(this.f.controls).forEach(field => {
        const control = this.f.get(field);
        control.markAsTouched({onlySelf: true});
    });
    } 
   if(this.f.valid){
     this.save();
   }
  }

  }
