export const formError = {

    'marque' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
    'matricule' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ]
   
};