import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutVehicComponent } from './ajout-vehic.component';

describe('AjoutVehicComponent', () => {
  let component: AjoutVehicComponent;
  let fixture: ComponentFixture<AjoutVehicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutVehicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutVehicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
