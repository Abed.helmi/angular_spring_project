import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanMissComponent } from './plan-miss.component';

describe('PlanMissComponent', () => {
  let component: PlanMissComponent;
  let fixture: ComponentFixture<PlanMissComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanMissComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanMissComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
