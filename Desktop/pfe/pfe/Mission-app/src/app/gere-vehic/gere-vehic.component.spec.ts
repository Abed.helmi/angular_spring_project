import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GereVehicComponent } from './gere-vehic.component';

describe('GereVehicComponent', () => {
  let component: GereVehicComponent;
  let fixture: ComponentFixture<GereVehicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GereVehicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GereVehicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
