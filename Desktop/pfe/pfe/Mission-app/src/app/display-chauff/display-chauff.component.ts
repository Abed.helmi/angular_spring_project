import { Component, OnInit } from '@angular/core';
import { Chauffeur } from '../Model/chauffeur';
import { ChauffeurService } from '../Services/chauffeur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-display-chauff',
  templateUrl: './display-chauff.component.html',
  styleUrls: ['./display-chauff.component.css']
})
export class DisplayChauffComponent implements OnInit {

  chauffeurs: Array<Chauffeur[]>;
  motcle:string="";

  constructor( private chauffeurservice: ChauffeurService, private route: Router) { }
  ngOnInit() {

    this.display();
  }


  display(){
    this.chauffeurservice.getChauffeurList().subscribe(
      data => { this.chauffeurs = data;}
    );
  }

  deleteChauffeur(id_chauffeur: number){
    let confirme=window.confirm('Est vous sure ?');
 

    if(confirme==true){

      this.chauffeurservice.deleteChauffeur(id_chauffeur).subscribe(
        data =>{
          console.log(data);
          confirme=window.confirm('Vous avez supprimé Chauffeur avec succès!');
          this.ngOnInit();
    

        },
        error => {
          alert("problème de supprussion");
        }
        );

        }
  }


  UpdateChauffeur(chauffeur){
    this.chauffeurservice.setter(chauffeur);
    this.route.navigateByUrl('/home/gererchaufferes/consulterchauffeur/modifierchauffeur');
  }
  
  newChauffeur(){
    let chauffeur = new Chauffeur();
    this.chauffeurservice.setter(chauffeur);
    this.route.navigateByUrl('home/gererchaufferes/consulterchauffeur/modifierchauffeur');
  }




dosearch(){
  this.chauffeurservice.getChauffch(this.motcle).subscribe(data=>{this.chauffeurs=data;
  console.log(this.chauffeurs);
  }
   ,err=>{console.log("erreur")});
  
  
}

chercher(){
this.dosearch();

}


}
