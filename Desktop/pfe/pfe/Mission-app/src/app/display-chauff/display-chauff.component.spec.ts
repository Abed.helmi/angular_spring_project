import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayChauffComponent } from './display-chauff.component';

describe('DisplayChauffComponent', () => {
  let component: DisplayChauffComponent;
  let fixture: ComponentFixture<DisplayChauffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayChauffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayChauffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
