import { Component, OnInit } from '@angular/core';
import { Statpass } from '../Model/statpass';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {formError} from './validation';
import { StatpassService } from '../Services/statpass.service';

@Component({
  selector: 'app-add-statpass',
  templateUrl: './add-statpass.component.html',
  styleUrls: ['./add-statpass.component.css']
})
export class AddStatpassComponent implements OnInit {
  stationpass : Statpass = new Statpass();
  message: string;
  f: FormGroup;

  validation_messages: any ;
  constructor(private formBuilder: FormBuilder, private router: Router, private statpassservice: StatpassService) { 
      

    this.validation_messages = formError;
    this.f = this.formBuilder.group({
      id_mission: ['', Validators.required], id_station_pasage: ['', Validators.required],ordre: ['', Validators.required] });
  }

  ngOnInit() {
  }

save(){

  this.statpassservice.create(this.stationpass).subscribe(
    data => {
      console.log(data);
      alert('Vous avez ajouté station de passage avec succés!');
      this.router.navigateByUrl("/home/planifiermission/Stationspassages");

    
    }, error => {
      
      alert("Problème Ajout");
    }
    
  );


}


  onSubmit(){

    if (this.f.invalid) {
      Object.keys(this.f.controls).forEach(field => {
        const control = this.f.get(field);
        control.markAsTouched({onlySelf: true});
    });
    } 

    if(this.f.valid){
      this.save();

    }
    


  }
}
