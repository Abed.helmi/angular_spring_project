import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStatpassComponent } from './add-statpass.component';

describe('AddStatpassComponent', () => {
  let component: AddStatpassComponent;
  let fixture: ComponentFixture<AddStatpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStatpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStatpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
