import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutChauffComponent } from './ajout-chauff.component';

describe('AjoutChauffComponent', () => {
  let component: AjoutChauffComponent;
  let fixture: ComponentFixture<AjoutChauffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutChauffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutChauffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
