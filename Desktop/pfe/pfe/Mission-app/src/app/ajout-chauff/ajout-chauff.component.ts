import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {formError} from './validation';
import { Chauffeur } from '../Model/chauffeur';
import { ChauffeurService } from '../Services/chauffeur.service';
@Component({
  selector: 'app-ajout-chauff',
  templateUrl: './ajout-chauff.component.html',
  styleUrls: ['./ajout-chauff.component.css']
})
export class AjoutChauffComponent implements OnInit {

  chauffeur : Chauffeur = new Chauffeur();

  f: FormGroup;
  message: string;
  validation_messages: any ;
  constructor(private formBuilder: FormBuilder, private router: Router, private chauffeurservice: ChauffeurService) { 
    this.validation_messages = formError;
    this.f = this.formBuilder.group({
     cin: ['', Validators.required],
      nom_chauffeur: ['', Validators.required],prenom_chauffeur: ['', Validators.required],login: ['',Validators.required]
      ,mot_de_passe: ['', Validators.required],
     
      email_chauffeur : new FormControl('' , Validators.compose([Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),Validators.required])),
    });
  }

  ngOnInit() {

   
  }


  /*newChauffeur(): void{
    this.chauffeur = new Chauffeur();
  }*/




save(){
  this.chauffeurservice.createChauffeur(this.chauffeur).subscribe(
    data => {
      console.log(data);
      alert('Vous avez ajouté Chauffeur avec succés!');
      this.router.navigateByUrl("/home/gererchaufferes");

    
    }, error => {
      
      console.log(JSON.parse(error._body).message);
      alert("Problème Ajout");
    }
    
  );
  //this.chauffeur = new Chauffeur();

}

  onSubmit(){

    if (this.f.invalid) {
      Object.keys(this.f.controls).forEach(field => {
        const control = this.f.get(field);
        control.markAsTouched({onlySelf: true});
    });
    } 

    if(this.f.valid){
      this.save();

    }
    


  }


}