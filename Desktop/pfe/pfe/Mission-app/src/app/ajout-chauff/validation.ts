export const formError = {
    
    'cin' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    

    'nom_chauffeur' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'prenom_chauffeur' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    
    'email_chauffeur' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
        {type: 'pattern',message:'email invalide'}
    ],
    'mot_de_passe' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ],
    'login' : [
        {type: 'required', message: 'Ceci est un champ obligatoire'},
    ]
   
   
   
   
};