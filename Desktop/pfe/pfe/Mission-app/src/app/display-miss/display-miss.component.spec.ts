import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayMissComponent } from './display-miss.component';

describe('DisplayMissComponent', () => {
  let component: DisplayMissComponent;
  let fixture: ComponentFixture<DisplayMissComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayMissComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayMissComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
