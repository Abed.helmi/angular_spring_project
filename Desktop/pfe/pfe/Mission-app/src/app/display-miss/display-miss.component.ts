import { Component, OnInit } from '@angular/core';
import { Mission } from '../Model/mission';
import { MissionService } from '../Services/mission.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-display-miss',
  templateUrl: './display-miss.component.html',
  styleUrls: ['./display-miss.component.css']
})
export class DisplayMissComponent implements OnInit {
  missions: Array<Mission[]>;
  motcle:string="";
  
  constructor(private missionservice : MissionService, private route: Router) { }

  ngOnInit() {
    this.display();

  }


  display(){
    this.missionservice.getMissionList().subscribe(
      data => { this.missions = data;}
    );
  }



  deleteMission(id_mission: number){
    let confirme=window.confirm('Est vous sure ?');
 

    if(confirme==true){

      this.missionservice.deleteMission(id_mission).subscribe(
        data =>{
          console.log(data);
    
          this.ngOnInit();
    
        },
        error => console.log(error));
        confirme=window.confirm('Vous avez supprimé Mission avec succès!');
    
        }
  }





  UpdateMission(mission){
    this.missionservice.setter(mission);
    this.route.navigateByUrl('home/planifiermission/consultermission/modifiermission');
  }
  
  newMission(){
    let mission = new Mission();
    this.missionservice.setter(mission);
    this.route.navigateByUrl('home/planifiermission/consultermission/modifiermission');
  }


  dosearch(){
    this.missionservice.getMissionch(this.motcle).subscribe(data=>{this.missions=data;
    console.log(this.missions);
    }
     ,err=>{console.log("erreur")});
    
    
  }
  
  chercher(){
  this.dosearch();
  }
}
