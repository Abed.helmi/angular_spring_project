import { Component, OnInit } from '@angular/core';
import { Statpass } from '../Model/statpass';
import { Router } from '@angular/router';
import { StatpassService } from '../Services/statpass.service';

@Component({
  selector: 'app-display-statpass',
  templateUrl: './display-statpass.component.html',
  styleUrls: ['./display-statpass.component.css']
})
export class DisplayStatpassComponent implements OnInit {
  statspass: Array<Statpass[]>;
  constructor( private statpassservice:StatpassService, private route : Router) { }

  ngOnInit() {
    this.dispaly();
  }

  dispaly(){
    this.statpassservice.getstatpasslist().subscribe(
      data => { this.statspass = data;}
    );
  }


  delte(id: number){
    let confirme=window.confirm('Est vous sure ?');
 

    if(confirme==true){

      this.statpassservice.delete(id).subscribe(
        data =>{
          console.log(data);
          confirme=window.confirm('Succès de suppression');
          this.ngOnInit();
    

        },
        error => {
          alert("Problème de suppression");
        }
        );

        }
  }

}
