import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayStatpassComponent } from './display-statpass.component';

describe('DisplayStatpassComponent', () => {
  let component: DisplayStatpassComponent;
  let fixture: ComponentFixture<DisplayStatpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayStatpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayStatpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
