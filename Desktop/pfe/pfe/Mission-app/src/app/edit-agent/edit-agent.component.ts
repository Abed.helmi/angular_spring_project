import { Component, OnInit } from '@angular/core';
import { Agent } from '../Model/agent';
import { AgentService } from '../Services/agent.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-agent',
  templateUrl: './edit-agent.component.html',
  styleUrls: ['./edit-agent.component.css']
})
export class EditAgentComponent implements OnInit {
  agent : Agent = new Agent();
  constructor(private agentservice: AgentService,private route: Router) { }

  newAgent(): void{
    this.agent = new Agent();
  }

  ngOnInit() {

    this.agent = this.agentservice.getter();

   
  }

  save(){
    this.agentservice.createAgent(this.agent).subscribe(
      data => {
        
        console.log(data);
        alert("Mise à jour effectuée ");
        this.route.navigateByUrl('/admin/consulterAgents');

        
      }, error => {
        console.log(error);
        alert('Problème de modification');

      }
    );
    }
  retour(){
    this.route.navigateByUrl("/admin/consulterAgents");
  }


  onSubmit()  {

    this.save();

  


  }
}
