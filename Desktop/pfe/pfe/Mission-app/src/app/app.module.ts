import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import{Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { MenuComponent } from './menu/menu.component';
import { PlanMissComponent } from './plan-miss/plan-miss.component';
import { HeaderComponent } from './header/header.component';
import { ImagesComponent } from './images/images.component';
import { GereVehicComponent } from './gere-vehic/gere-vehic.component';
import { GereChauffComponent } from './gere-chauff/gere-chauff.component';
import { SuivMissComponent } from './suiv-miss/suiv-miss.component';
import { BarreLeftComponent } from './barre-left/barre-left.component';
import { DisplayMissComponent } from './display-miss/display-miss.component';
import { AjoutermissionComponent } from './ajoutermission/ajoutermission.component';
import { AjoutVehicComponent } from './ajout-vehic/ajout-vehic.component';
import { ConsultVehicComponent } from './consult-vehic/consult-vehic.component';
import { AjoutChauffComponent } from './ajout-chauff/ajout-chauff.component';
import { DisplayChauffComponent } from './display-chauff/display-chauff.component';
import { ProfileComponent } from './profile/profile.component';
import { SuperAdminComponent } from './super-admin/super-admin.component';
import { HeaderSaComponent } from './header-sa/header-sa.component';
import { AjoutAdminComponent } from './ajout-admin/ajout-admin.component';
import { DisplayAdminComponent } from './display-admin/display-admin.component';
import { EditAgentComponent } from './edit-agent/edit-agent.component';
import { LogoutSaComponent } from './logout-sa/logout-sa.component';
import { EditChauffeurComponent } from './edit-chauffeur/edit-chauffeur.component';
import { EditVehiculeComponent } from './edit-vehicule/edit-vehicule.component';
import { AuthguardService } from './Authentification/authguard.service';
import { EditMissionComponent } from './edit-mission/edit-mission.component';
import { GererStationsComponent } from './gerer-stations/gerer-stations.component';
import { AjoutStationComponent } from './ajout-station/ajout-station.component';
import { DisplayStationComponent } from './display-station/display-station.component';
import { EditStationComponent } from './edit-station/edit-station.component';
import { AddStatpassComponent } from './add-statpass/add-statpass.component';
import { DisplayStatpassComponent } from './display-statpass/display-statpass.component';
const routes:Routes=[
  {path:'admin',component:SuperAdminComponent,canActivate:[AuthguardService],
children: [
  {path:'consulterAgents',component:DisplayAdminComponent,
  children: [
    {path:'modifierAgents',component:EditAgentComponent}
  ]
}
]
},
  {path:'auth',component:AuthComponent},
  {path:'suivremission',component:SuivMissComponent},
  { path:'home',component:HomeComponent,canActivate:[AuthguardService],
  
  
children: [
  {path:'gererstations',component:GererStationsComponent,
  children: [
{path: 'consulterstation', component:DisplayStationComponent,
children: [{path:'modifierstation',component:EditStationComponent}]},
{
  path:'ajouterstation',component: AjoutStationComponent}
  ]
  
    }  ,

  {path:'profile',component:ProfileComponent},
  {path:'planifiermission',component:PlanMissComponent,
children: [
  {path:'consultermission',component:DisplayMissComponent,
children: [{path : 'modifiermission',component:EditMissionComponent}]
},
  { path:'ajoutermission',component:AjoutermissionComponent},
  {path:'Stationspassages',component:AddStatpassComponent,
children: [{
  path: 'consulter',component:DisplayStatpassComponent
}]
}
]

},
  {path: 'gerervéhicules',component:GereVehicComponent,
  children: [
    {path:'consultervehicule',component:ConsultVehicComponent,
  children:[{
    path: 'modifiervehicule',component:EditVehiculeComponent
  }]
  
  },
    { path:'ajoutervehicule',component:AjoutVehicComponent}
  ]

},
  {path: 'gererchaufferes',component:GereChauffComponent,


  children: [
    {path:'consulterchauffeur',component:DisplayChauffComponent,
      
    children: [
      {path: 'modifierchauffeur',component: EditChauffeurComponent}
    ]
  },
    { path:'ajouterchauffeur',component:AjoutChauffComponent}
  ]

},
  {path: 'suivremission',component:SuivMissComponent,canActivateChild:[AuthguardService]},
]},

  { path:'', redirectTo:'/auth', pathMatch:'full'}
  
];
@NgModule({
  declarations: [
    
    AppComponent,
    HomeComponent,
    AuthComponent,
    MenuComponent,
    PlanMissComponent,
    HeaderComponent,
    ImagesComponent,
    GereVehicComponent,
    GereChauffComponent,
    SuivMissComponent,
    BarreLeftComponent,
    DisplayMissComponent,
    AjoutermissionComponent,
    AjoutVehicComponent,
    ConsultVehicComponent,
    AjoutChauffComponent,
    DisplayChauffComponent,
    ProfileComponent,
    SuperAdminComponent,
    HeaderSaComponent,
    AjoutAdminComponent,
    DisplayAdminComponent,
    EditAgentComponent,
    LogoutSaComponent,
    EditChauffeurComponent,
    EditVehiculeComponent,
    EditMissionComponent,
    GererStationsComponent,
    AjoutStationComponent,
    DisplayStationComponent,
    EditStationComponent,
    AddStatpassComponent,
    DisplayStatpassComponent,
  ],
  imports: [
    AngularFontAwesomeModule,

    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
  ],
  providers: [AuthguardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
