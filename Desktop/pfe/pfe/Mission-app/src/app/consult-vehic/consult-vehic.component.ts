import { Component, OnInit, Input } from '@angular/core';
import { Vehicule } from '../Model/vehicule';
import { VehiculeService } from '../Services/vehicule.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-consult-vehic',
  templateUrl: './consult-vehic.component.html',
  styleUrls: ['./consult-vehic.component.css']
})
export class ConsultVehicComponent implements OnInit {
 
vehicules: Array<Vehicule[]>
motcle:string="";
  constructor( private vehiculeservice: VehiculeService, private route: Router, ) { }

  ngOnInit() {

    this.display();
    
  }


 
display(){
  
  this.vehiculeservice.getVehiculeList().subscribe(
    data => { this.vehicules = data;}
  );
}

deleteVehicule(id_vehicule: number){

  let confirme=window.confirm('Est vous sure ?');
 

  if(confirme==true){

    this.vehiculeservice.deleteVehicule(id_vehicule).subscribe(
      data =>{
        console.log(data);
        confirme=window.confirm('Vous avez supprimé Vehicule  avec succès!');
        this.ngOnInit();
  
      },
      error =>{
        alert("problème de supprussion");

      }
      );

  
      }
}

UpdateVehicule(vehicule){
  this.vehiculeservice.setter(vehicule);
  this.route.navigateByUrl('/home/gerervéhicules/consultervehicule/modifiervehicule');
}

newVehicule(){
  let vehicule = new Vehicule();
  this.vehiculeservice.setter(vehicule);
  this.route.navigateByUrl('home/gerervéhicules/consultervehicule/modifiervehicule');
}
    



 dosearch(){
      this.vehiculeservice.getvehicule(this.motcle).subscribe(data=>{this.vehicules=data;
        console.log(this.vehicules);
        
     
       }
       ,err=>{console.log("erreur")})
      
      
    }
    
    chercher(){
    this.dosearch();
    
    }



  }