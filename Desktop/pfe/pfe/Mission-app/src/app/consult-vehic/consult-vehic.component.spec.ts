import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultVehicComponent } from './consult-vehic.component';

describe('ConsultVehicComponent', () => {
  let component: ConsultVehicComponent;
  let fixture: ComponentFixture<ConsultVehicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultVehicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultVehicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
