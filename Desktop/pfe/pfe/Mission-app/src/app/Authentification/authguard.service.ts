import { Injectable } from '@angular/core';
import { Router, CanActivate,ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService  implements CanActivate{

  constructor(private route: Router, private authservic: AuthService) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(this.authservic.isAdminLoggedIn()||(this.authservic.isAgentLoggedIn()))
    return true;
    this.route.navigateByUrl("/auth");
    return false;
  }
  /*canActivateAgent(route:ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(this.authservic.isAgentLoggedIn())
    return true;
    this.route.navigateByUrl("/auth");
    return false;
  }*/
}
