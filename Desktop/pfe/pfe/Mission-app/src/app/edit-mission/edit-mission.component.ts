import { Component, OnInit } from '@angular/core';
import { MissionService } from '../Services/mission.service';
import { Router } from '@angular/router';
import { Mission } from '../Model/mission';

@Component({
  selector: 'app-edit-mission',
  templateUrl: './edit-mission.component.html',
  styleUrls: ['./edit-mission.component.css']
})
export class EditMissionComponent implements OnInit {
mission : Mission = new Mission();
  constructor(private missionservice:MissionService, private route:Router) { }

  ngOnInit() {

    this.mission= this.missionservice.getter();
  }

  save(){
    this.missionservice.createMission(this.mission).subscribe(
      data => {console.log(data);
      alert("Mise à jour effectuée ");
      this.route.navigateByUrl('/home/planifiermission/consultermission');

    },
       error => {
        
      console.log(error);
      alert("Problème de modification");
              
       }
    );   
    }





    retour(){
      this.route.navigateByUrl("/home/planifiermission/consultermission");
  
    }
  
  
    onSubmit()  {
      this.save();
  
  
    }
  



}
