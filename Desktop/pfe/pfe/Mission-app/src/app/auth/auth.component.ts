import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {formError} from './validation';
import { AuthService } from '../Authentification/auth.service';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  f: FormGroup;
  message: string;
  validation_messages: any ;
  login=''
  password='';
  invalidlogin= false;
  constructor(  private formBuilder: FormBuilder, private router: Router,private loginservice: AuthService) {
    
   }

  ngOnInit() {
    this.validation_messages = formError;
    this.f = this.formBuilder.group({
      login: ['', Validators.required],
      password : ['', Validators.required]
      
    
    });
  }
  
  checkLogin(){
if (this.loginservice.authenticate(this.login, this.password))
{
  this.router.navigateByUrl("/admin")
  this.invalidlogin = false
}else if(this.loginservice.authenticateagent(this.login, this.password)){
  this.router.navigateByUrl("/home")
  this.invalidlogin = false

}
else{
  this.invalidlogin = true
  alert("auhentification invalide");
}






  }




  onSubmit()
  { if (this.f.invalid) {
    Object.keys(this.f.controls).forEach(field => {
      const control = this.f.get(field);
      control.markAsTouched({onlySelf: true});

     
  });
  } 
 if (this.f.valid){
   this.checkLogin();
 }
  }


}


