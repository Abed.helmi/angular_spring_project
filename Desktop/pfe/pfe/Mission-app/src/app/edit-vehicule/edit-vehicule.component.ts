import { Component, OnInit } from '@angular/core';
import { Vehicule } from '../Model/vehicule';
import { VehiculeService } from '../Services/vehicule.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-vehicule',
  templateUrl: './edit-vehicule.component.html',
  styleUrls: ['./edit-vehicule.component.css']
})
export class EditVehiculeComponent implements OnInit {

  vehicule : Vehicule = new Vehicule();

  constructor( private vehiculeservice : VehiculeService, private route: Router) { }

  ngOnInit() {
    this.vehicule = this.vehiculeservice.getter();

  }


  save(){
    this.vehiculeservice.createVehicule(this.vehicule).subscribe(
      data => {
        console.log(data);
        alert("Mise à jour effectuée");
        this.route.navigateByUrl('/home/gerervéhicules/consultervehicule');
      }, error => {
        
        console.log(error);
        alert("Problème de modification");}
      
    );
  }


  retour(){
    this.route.navigateByUrl("/home/gerervéhicules/consultervehicule");
  } 



  onSubmit()  {
    this.save();


  }

}
