import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GererStationsComponent } from './gerer-stations.component';

describe('GererStationsComponent', () => {
  let component: GererStationsComponent;
  let fixture: ComponentFixture<GererStationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GererStationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GererStationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
