package org.aci.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mission_station")
public class Mission_station {
	
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	@Column(name="id")
	@Id
	private Long id;
	
	
	@NotNull
	@Column(name="id_mission",nullable=false)
	private Long id_mission;
	
	
	@NotNull
	@Column(name="id_station_passage",nullable=false)
	private Long id_station_pasage ;
	
	
	
	@Column(name="ordre",nullable=false)
	@NotNull
	private int ordre;



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Long getId_mission() {
		return id_mission;
	}



	public void setId_mission(Long id_mission) {
		this.id_mission = id_mission;
	}



	public Long getId_station_pasage() {
		return id_station_pasage;
	}



	public void setId_station_pasage(Long id_station_pasage) {
		this.id_station_pasage = id_station_pasage;
	}



	public int getOrdre() {
		return ordre;
	}



	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}



	@Override
	public String toString() {
		return "Mission_station [id=" + id + ", id_mission=" + id_mission + ", id_station_pasage=" + id_station_pasage
				+ ", ordre=" + ordre + "]";
	}



	public Mission_station() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Mission_station(@NotNull Long id_mission, @NotNull Long id_station_pasage, @NotNull int ordre) {
		super();
		this.id_mission = id_mission;
		this.id_station_pasage = id_station_pasage;
		this.ordre = ordre;
	}


}
