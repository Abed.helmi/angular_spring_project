package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.MissionRepository;
import org.aci.entities.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/v1")
public class MissionRestService {

	
	@Autowired
	private MissionRepository missionrep;
	
	
	//ajouter mission
	
	 @PostMapping("/missions")
	   public Mission createMission(@Valid @RequestBody Mission mision) {
    return missionrep.save(mision);
	
}
     
	 
	 //consulter les missions
	 @GetMapping("/missions")
	  public List<Mission> getMissionList() {
	 
	    return missionrep.findAll();
	    		
	  }
	 

	 //supprimer agent
	   @DeleteMapping("/missions/{id_mission}")
	   public void deleteMission(@PathVariable Long id_mission) {
	    	
	    	missionrep.deleteById(id_mission);
	    }
	 
	   

	   //modifier 
	   @PutMapping("/missions{id_mission}")
	   public Mission UpdateMission(@PathVariable("id_mission") @RequestBody Mission mission)
	   {
	   return missionrep.save(mission);
	   }
	 
	 
	   //Chercher

	   
	   @GetMapping("/cherchermissions")
		  public List<Mission> chercher(
				  
				  @RequestParam(name="mc",defaultValue="") String mc )
				
				   
		  {
		   return missionrep.chercher("%"+mc+"%");
		  }
	   
	   
}



