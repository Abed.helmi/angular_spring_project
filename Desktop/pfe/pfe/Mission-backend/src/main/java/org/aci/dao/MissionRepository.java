package org.aci.dao;

import java.util.List;

import org.aci.entities.Mission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MissionRepository extends JpaRepository<Mission, Long>{
	@Query("select c from Mission c where c.id_mission like :x ")
	public List<Mission> chercher(@Param("x")String mc);
	

}
