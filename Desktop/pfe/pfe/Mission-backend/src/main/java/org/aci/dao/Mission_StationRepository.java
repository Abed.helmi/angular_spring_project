package org.aci.dao;

import org.aci.entities.Mission_station;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Mission_StationRepository extends JpaRepository<Mission_station, Long>{

}
