package org.aci.entities;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="vehicule")
public class Vehicule {
	
	
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_vehicule")
	private Long id_vehicule ;
    
	@NotNull
	@Column(name="marque",nullable=false)
	private String marque ;
	
	@NotNull
	@Column(name="matricule",nullable=false,unique=true)
	private String matricule;
	
	@Column(name="chemin_gris_carte")
	private String chemin_gris_carte ;
	
	@Column(name="model")
	private String model ;
	
	
   @Column(name="sous_marque")
   private String sous_marque;
   

   
   @Column(name="etat")
   private String etat  ;
   


@Override
public String toString() {
	return "Vehicule [id_vehicule=" + id_vehicule + ", marque=" + marque + ", matricule=" + matricule
			+ ", chemin_gris_carte=" + chemin_gris_carte + ", model=" + model + ", sous_marque=" + sous_marque
			+ ", vitesse_maximale="  + ", etat=" + etat + "]";
}


public Vehicule() {
	super();
	// TODO Auto-generated constructor stub
}

public Long getId_vehicule() {
	return id_vehicule;
}

public void setId_vehicule(Long id_vehicule) {
	this.id_vehicule = id_vehicule;
}

public String getMarque() {
	return marque;
}

public void setMarque(String marque) {
	this.marque = marque;
}

public String getMatricule() {
	return matricule;
}

public void setMatricule(String matricule) {
	this.matricule = matricule;
}

public String getChemin_gris_carte() {
	return chemin_gris_carte;
}

public void setChemin_gris_carte(String chemin_gris_carte) {
	this.chemin_gris_carte = chemin_gris_carte;
}

public String getModel() {
	return model;
}

public void setModel(String model) {
	this.model = model;
}

public String getSous_marque() {
	return sous_marque;
}

public void setSous_marque(String sous_marque) {
	this.sous_marque = sous_marque;
}

public String getEtat() {
	return etat;
}

public void setEtat(String etat) {
	this.etat = etat;
}

public Vehicule(@NotNull String marque, @NotNull String matricule, String chemin_gris_carte, String model,
		String sous_marque, String etat) {
	super();
	this.marque = marque;
	this.matricule = matricule;
	this.chemin_gris_carte = chemin_gris_carte;
	this.model = model;
	this.sous_marque = sous_marque;
	this.etat = etat;
}





}
