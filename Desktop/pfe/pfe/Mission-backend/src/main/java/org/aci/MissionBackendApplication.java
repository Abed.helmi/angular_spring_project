package org.aci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MissionBackendApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MissionBackendApplication.class, args);
	}

}
