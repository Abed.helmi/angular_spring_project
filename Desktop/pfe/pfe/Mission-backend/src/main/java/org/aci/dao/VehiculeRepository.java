package org.aci.dao;

import java.util.List;

import org.aci.entities.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VehiculeRepository extends JpaRepository<Vehicule, Long>{
	
	
	@Query("select c from Vehicule c where c.marque like :x ")
	public List<Vehicule> chercher(@Param("x")String mc);
	   
	
}
