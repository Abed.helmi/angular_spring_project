package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.VehiculeRepository;
import org.aci.entities.Vehicule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/v1")
public class VehiculeRestService {

	
	@Autowired
	private  VehiculeRepository vehiculerep;
	
	
	

	//ajouter vehicule
	
	@PostMapping("/vehicules")
	   public Vehicule createVehicule(@Valid @RequestBody Vehicule vehicule) {
   return vehiculerep.save(vehicule);
		      
		    }
	

	 //consulter les vehicules
	 @GetMapping("/vehicules")
	  public List<Vehicule> getVehiculeList() {
	 
	    return vehiculerep.findAll();
	    		
	  }
	 

	 //supprimer vehicule
	   @DeleteMapping("/vehicules/{id_vehicule}")
	   public void deleteVehicule(@PathVariable Long id_vehicule) {
	    	
	    	vehiculerep.deleteById(id_vehicule);
	    }
	   

	   
	   //modifier 
	   @PutMapping("/vehicules{id_vehicule}")
	   public Vehicule UpdateVehicule(@PathVariable("id_vehicule") @RequestBody Vehicule vehicule)
	   {
	   return vehiculerep.save(vehicule);
	   }
	   
        
	   
	   @GetMapping("/cherchervehicules")
		  public List <Vehicule> chercher(
				  
				  @RequestParam(name="mc",defaultValue="") String mc )

		  {
		   return vehiculerep.chercher("%"+mc+"%");
				  
				  
		  }  
	 
	
	
}
