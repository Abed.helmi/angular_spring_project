package org.aci.entities;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



@Entity
@Table(name="chauffeur")
public class Chauffeur extends Employe{ 
	

@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_chauffeur ")
	private Long id_chauffeur ;
	
	
	@NotNull
	@Column(name="cin", nullable=false, unique=true)
	private int cin ;

	
	
	@NotNull
	@Column(name="nom_chauffeur",nullable=false)
	private String nom_chauffeur;
	
	@NotNull
	@Column(name="prenom_chauffeur",nullable=false)
    private String prenom_chauffeur ;
	
	@Column(name="email_chauffeur",nullable=false,unique=true)
	private String email_chauffeur;
	
	@NotNull
	@Column(name="login",nullable=false,unique=true)
	private String login;
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name="mot_de_passe",nullable=false)
	private String mot_de_passe ;
	
	
	@Column(name="tel")
	private int tel;
	
	@Column(name="sang_groupe")
	private String sang_groupe;

	
	
	

	public Long getId_chauffeur() {
		return id_chauffeur;
	}

	public void setId_chauffeur(Long id_chauffeur) {
		this.id_chauffeur = id_chauffeur;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}


	public String getNom_chauffeur() {
		return nom_chauffeur;
	}

	public void setNom_chauffeur(String nom_chauffeur) {
		this.nom_chauffeur = nom_chauffeur;
	}

	public String getPrenom_chauffeur() {
		return prenom_chauffeur;
	}

	public void setPrenom_chauffeur(String prenom_chauffeur) {
		this.prenom_chauffeur = prenom_chauffeur;
	}

	public String getEmail_chauffeur() {
		return email_chauffeur;
	}

	public void setEmail_chauffeur(String email_chauffeur) {
		this.email_chauffeur = email_chauffeur;
	}

	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getSang_groupe() {
		return sang_groupe;
	}

	public void setSang_groupe(String sang_groupe) {
		this.sang_groupe = sang_groupe;
	}

	public Chauffeur(@NotNull int cin, @NotNull String nom_chauffeur,
			@NotNull String prenom_chauffeur, String email_chauffeur, @NotNull String login, String mot_de_passe,
			int tel, String sang_groupe) {
		super();
		this.cin = cin;
		this.nom_chauffeur = nom_chauffeur;
		this.prenom_chauffeur = prenom_chauffeur;
		this.email_chauffeur = email_chauffeur;
		this.login = login;
		this.mot_de_passe = mot_de_passe;
		this.tel = tel;
		this.sang_groupe = sang_groupe;
	}

	public Chauffeur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Chauffeur(long id_empl, int cin, String nom, String prenom, String email, int tel, String login,
			String mot_de_passe) {
		super(id_empl, cin, nom, prenom, email, tel, login, mot_de_passe);
		// TODO Auto-generated constructor stub
	}
	





	
	

}
