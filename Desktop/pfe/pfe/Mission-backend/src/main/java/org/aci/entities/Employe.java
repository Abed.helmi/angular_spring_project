package org.aci.entities;

public class   Employe {
	
	private  long id_empl;
	private int cin;

	private String nom;
	private String prenom;
	private String email;
	private int tel;
	private String login;
	private String mot_de_passe;
	public Employe() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Employe(long id_empl, int cin, String nom, String prenom, String email, int tel, String login,
			String mot_de_passe) {
		super();
		this.id_empl = id_empl;
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.tel = tel;
		this.login = login;
		this.mot_de_passe = mot_de_passe;
	}
	@Override
	public String toString() {
		return "Employe [id_empl=" + id_empl + ", cin=" + cin + ", nom=" + nom + ", prenom=" + prenom + ", email="
				+ email + ", tel=" + tel + ", login=" + login + ", mot_de_passe=" + mot_de_passe + "]";
	}
	public long getId_empl() {
		return id_empl;
	}
	public void setId_empl(long id_empl) {
		this.id_empl = id_empl;
	}
	public int getCin() {
		return cin;
	}
	public void setCin(int cin) {
		this.cin = cin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMot_de_passe() {
		return mot_de_passe;
	}
	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	};

}
