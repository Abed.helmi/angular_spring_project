package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.ChauffeurRepository;
import org.aci.entities.Chauffeur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")

@RestController
@RequestMapping("api/v1")
public class ChauffeurRestService {
	
	
	@Autowired
	private  ChauffeurRepository chaufrep ;
	
	
	//ajouter chauffeur
	
	@PostMapping("/chauffeurs")
	   public Chauffeur createChauffeur(@Valid @RequestBody Chauffeur chauffeur) {
   return chaufrep.save(chauffeur);
   
		      
		    }
	
	
	 //consulter les chauffeurs
	 @GetMapping("/chauffeurs")
	  public List<Chauffeur> getChauffeurList() {
	 
	    return chaufrep.findAll();
	    		
	  }

	 //supprimer chauffeur
	   @DeleteMapping("/chauffeurs/{id_chauffeur}")
	   public void deleteChauffeur(@PathVariable Long id_chauffeur) {
	    	
	    	chaufrep.deleteById(id_chauffeur);
	    }
	   
	   

	   
	   //modifier 
	   @PutMapping("/chauffeurs{id_chauffeur}")
	   public Chauffeur UpdateChauffeur(@PathVariable("id_chauffeur") @RequestBody Chauffeur chauffeur)
	   {
	   return chaufrep.save(chauffeur);
	   }
	   
	   //cherher
	   
	   @GetMapping("/chercherchauffeurs")
		  public List<Chauffeur> chercher(
				  
				  @RequestParam(name="mc",defaultValue="") String mc )
				
				   
		  {
		   return chaufrep.chercher("%"+mc+"%");
		  }
	   
	

}
