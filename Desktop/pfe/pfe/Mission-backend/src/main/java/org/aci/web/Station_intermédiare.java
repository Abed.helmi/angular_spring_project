package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.Mission_StationRepository;
import org.aci.entities.Mission_station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")

@RestController
@RequestMapping("api/v1")
public class Station_intermédiare {

	
	@Autowired
	private Mission_StationRepository sm;
	
	//ajouter

	 @PostMapping("/stationspassages")
	   public Mission_station create(@Valid @RequestBody Mission_station stationpass) {
     return sm.save(stationpass);
		      
		    }
	 
	 //consulter
	 @GetMapping("/stationspassages")
	  public List<Mission_station> getstatpasslist() {
	 
	    return sm.findAll();
	  }
	  
	  //supprimer stations
	   @DeleteMapping("/stationspassages/{id}")
	   public void delete(@PathVariable Long id) {
	    	
	    	sm.deleteById(id);
	    }
	   
	
}
