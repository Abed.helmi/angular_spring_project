package org.aci.dao;

import java.util.List;

import org.aci.entities.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AgentRepository extends JpaRepository<Agent, Long>{
	
@Query("select c from Agent c where c.nom like :x ")
public List<Agent> chercher(@Param("x")String mc);

}
