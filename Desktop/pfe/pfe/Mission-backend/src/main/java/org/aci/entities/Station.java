package org.aci.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="station")
public class Station {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_station")
	private Long id_station;
	

	
	@NotNull
	@Column(name="adresse",nullable=false)
	private String adresse;
	
	
	@NotNull
	@Column(name="longitude",nullable=false)
	private double longitude;
	
	
	@NotNull
	@Column(name="altitude",nullable=false)
	private double altitude;
	
	@NotNull
	@Column(name="nom",nullable=false)
	private String nom;
	
	@Column(name="niveau_zoom")
	private Long niveau_zoom;
	

	public Station() {
		super();
		// TODO Auto-generated constructor stub
	}




	public Station(@NotNull String adresse, @NotNull double longitude, @NotNull double altitude, String nom,
			Long niveau_zoom) {
		super();
		this.adresse = adresse;
		this.longitude = longitude;
		this.altitude = altitude;
		this.nom = nom;
		this.niveau_zoom = niveau_zoom;
	}




	@Override
	public String toString() {
		return "Station [id_station=" + id_station + ", adresse=" + adresse + ", longitude=" + longitude + ", altitude="
				+ altitude + ", nom=" + nom + ", niveau_zoom=" + niveau_zoom + "]";
	}


	public Long getId_station() {
		return id_station;
	}


	public void setId_station(Long id_station) {
		this.id_station = id_station;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public double getAltitude() {
		return altitude;
	}


	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Long getNiveau_zoom() {
		return niveau_zoom;
	}


	public void setNiveau_zoom(Long niveau_zoom) {
		this.niveau_zoom = niveau_zoom;
	}


	

}
