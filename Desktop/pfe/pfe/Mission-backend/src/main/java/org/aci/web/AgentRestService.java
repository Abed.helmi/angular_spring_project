package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.AgentRepository;
import org.aci.entities.Agent;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:4200")

@RestController
@RequestMapping("api/v1")
public class AgentRestService {
	
	@Autowired
	private AgentRepository agentrep;
	
	
	//ajouter agent
	
	 @PostMapping("/agents")
	   public Agent createAgent(@Valid @RequestBody Agent agent) {
      return agentrep.save(agent);
		      
		    }
	 
	 //consulter les agents
	 @GetMapping("/agents")
	  public List<Agent> getAgentList() {
	 
	    return agentrep.findAll();
	    		
	  }
	 
	 //supprimer agent
	   @DeleteMapping("/agents/{id}")
	   public void deleteAgent(@PathVariable Long id) {
	    	
	    	agentrep.deleteById(id);
	    }
	   
	   
	   //modifier 
	   @PutMapping("/agents{id}")
	   public Agent UpdateAgent(@PathVariable("id") @RequestBody Agent agent)
	   {
	   return agentrep.save(agent);
	   }
	   
	   //chercher
	   
	   @GetMapping("/chercheragents")
		  public List<Agent> chercher(
				  
				  @RequestParam(name="mc",defaultValue="") String mc )
				
				   
		  {
		   return agentrep.chercher("%"+mc+"%");
		  }
				  

		  
	  
}

