package org.aci.dao;

import java.util.List;

import org.aci.entities.Chauffeur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ChauffeurRepository extends JpaRepository<Chauffeur, Long>{

	
	@Query("select c from Chauffeur c where c.nom_chauffeur like :x ")
	public List<Chauffeur> chercher(@Param("x")String mc);
}
