package org.aci.entities;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;



@Entity
@Table(name="mission")
public class Mission {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_mission")
	@Id
	private Long id_mission ;
	
	@NotNull
     @Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_debut", nullable=false)
	private Calendar  date_debut ;
	
	
	@NotNull
     @Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_arrive",nullable=false)
	private Calendar date_arrive;
	
	@NotFound
	@Column(name="chauffeur_id")
	private  int chauffeur_id;
	
	@NotFound
	@Column(name="vehicule_id")
	private  int vehicule_id;
	
	@NotNull

	@Column(name="id_station_depart",nullable=false)
	private long id_station_depart;
	@NotNull

	@Column(name="id_station_arrive",nullable=false)
	private long id_station_arrive;
	public Long getId_mission() {
		return id_mission;
	}
	public void setId_mission(Long id_mission) {
		this.id_mission = id_mission;
	}
	public Calendar getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Calendar date_debut) {
		this.date_debut = date_debut;
	}
	public Calendar getDate_arrive() {
		return date_arrive;
	}
	public void setDate_arrive(Calendar date_arrive) {
		this.date_arrive = date_arrive;
	}
	public int getChauffeur_id() {
		return chauffeur_id;
	}
	public void setChauffeur_id(int chauffeur_id) {
		this.chauffeur_id = chauffeur_id;
	}
	public int getVehicule_id() {
		return vehicule_id;
	}
	public void setVehicule_id(int vehicule_id) {
		this.vehicule_id = vehicule_id;
	}
	public long getId_station_depart() {
		return id_station_depart;
	}
	public void setId_station_depart(long id_station_depart) {
		this.id_station_depart = id_station_depart;
	}
	public long getId_station_arrive() {
		return id_station_arrive;
	}
	public void setId_station_arrive(long id_station_arrive) {
		this.id_station_arrive = id_station_arrive;
	}
	@Override
	public String toString() {
		return "Mission [id_mission=" + id_mission + ", date_debut=" + date_debut + ", date_arrive=" + date_arrive
				+ ", chauffeur_id=" + chauffeur_id + ", vehicule_id=" + vehicule_id + ", id_station_depart="
				+ id_station_depart + ", id_station_arrive=" + id_station_arrive + "]";
	}
	public Mission(@NotNull Calendar date_debut, @NotNull Calendar date_arrive, int chauffeur_id, int vehicule_id,
			@NotNull long id_station_depart, @NotNull long id_station_arrive) {
		super();
		this.date_debut = date_debut;
		this.date_arrive = date_arrive;
		this.chauffeur_id = chauffeur_id;
		this.vehicule_id = vehicule_id;
		this.id_station_depart = id_station_depart;
		this.id_station_arrive = id_station_arrive;
	}
	public Mission() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	
	
	
	
	
}
