package org.aci.entities;


import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@javax.persistence.Entity
@Table(name =" agent")
public class Agent extends Employe{
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name=" prenom", nullable=false)
	private String prenom;

	@NotNull
	@Column(name="cin", nullable=false,unique=true)
	private int cin;
	
	@NotNull
	@Column(name="nom", nullable=false)
	private String nom;
	

@NotNull
	@Column(name="email", nullable=false,unique=true)
	private String email;
	
	@NotNull
	@Column(name="login", nullable=false, unique=true)
	private String login;
	
	@NotNull
	@Column(name="mot_de_passe", nullable=false,unique=true)
	private String mot_de_passe;
	
	
	@Column(name="tel")
	private int tel;
	 
	@Temporal(TemporalType.DATE)
	@Column(name="date_cration")
	private Calendar date_creation ;
	
	
	
	

	public int getTel_chauffeur() {
		return tel;
	}

	public void setTel_chauffeur(int tel_chauffeur) {
		this.tel= tel_chauffeur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom= nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}



	
	@Override
	public String toString() {
		return "Agent [id=" + id + ", prenom=" + prenom + ", cin=" + cin + ", nom=" + nom + ", email=" + email
				+ ", login=" + login + ", mot_de_passe=" + mot_de_passe + ", tel=" + tel + ", date_creation="
				+ date_creation + "]";
	}

	public Agent(@NotNull String prenom_agent, @NotNull int cin, @NotNull String nom_agent, @NotNull String email,
			@NotNull String login, @NotNull String mot_de_passe, int tel_chauffeur, Calendar date_creation) {
		super();
		this.prenom= prenom_agent;
		this.cin = cin;
		this.nom= nom_agent;
		this.email = email;
		this.login = login;
		this.mot_de_passe = mot_de_passe;
		this.tel = tel_chauffeur;
		this.date_creation = date_creation;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public Calendar getDate_creation() {
		return date_creation;
	}

	public void setDate_creation(Calendar date_creation) {
		this.date_creation = date_creation;
	}

	public Agent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Agent(long id_empl, int cin, String nom, String prenom, String email, int tel, String login,
			String mot_de_passe) {
		super(id_empl, cin, nom, prenom, email, tel, login, mot_de_passe);
		// TODO Auto-generated constructor stub
	}

	
	
	
	

}
