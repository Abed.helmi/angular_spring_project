package org.aci.dao;

import java.util.List;

import org.aci.entities.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StationRepository  extends JpaRepository<Station, Long>{

	@Query("select c from Station c where c.nom like :x ")
	public List<Station> chercher(@Param("x")String mc);
	
}
