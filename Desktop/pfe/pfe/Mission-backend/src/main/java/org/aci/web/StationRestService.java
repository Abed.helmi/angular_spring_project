package org.aci.web;

import java.util.List;

import javax.validation.Valid;

import org.aci.dao.StationRepository;
import org.aci.entities.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")

@RestController
@RequestMapping("api/v1")
public class StationRestService {

	

	@Autowired
	private StationRepository statrep;
	
	//ajouter station
	
	 @PostMapping("/stations")
	   public Station createSattion(@Valid @RequestBody Station station) {
    return statrep.save(station);
    
    
	 }
	 //consulter les stations
	 @GetMapping("/stations")
	  public List<Station> getStationList() {
	 
	    return statrep.findAll();
	    		
	  }
	 
	 

	 //supprimer stations
	   @DeleteMapping("/stations/{id_station}")
	   public void deleteStation(@PathVariable Long id_station) {
	    	
	    	statrep.deleteById(id_station);
	    }
	   

	   //modifier 
	   @PutMapping("/stations{id_station}")
	   public Station UpdateStation(@PathVariable("id_station") @RequestBody Station station)
	   {
	   return statrep.save(station);
	   }
	   
   //cherher
	   
	   @GetMapping("/chercherstations")
		  public List<Station> chercher(
				  
				  @RequestParam(name="mc",defaultValue="") String mc )
				
				   
		  {
		   return statrep.chercher("%"+mc+"%");
		  }
	   
		      
		    
	

}
